package routers

import (
	"addPlan/handler"
	"addPlan/router/api"
	"addPlan/router/apn"
	"github.com/gocraft/web"
)

func InitRouter() *web.Router {
	rootRouter := web.New(handler.RootContext{}).
		Middleware((*handler.RootContext).PrePackMiddleware).
		Middleware((*handler.RootContext).HandlePanicMiddleware).
		Middleware((*handler.RootContext).Acao)

	// api
	api.ApiRouter(rootRouter)

	// apn
	apn.ApnRouter(rootRouter)

	return rootRouter
}
