package apn

import (
	"addPlan/handler"
	"github.com/gocraft/web"
)

func ApnRouter(router *web.Router) {
	apnRouter := router.Subrouter(handler.Apn{}, "/apn")
	// 官网学堂
	apnRouter.Post("/school/read", (*handler.Apn).AddReadNumberHandler)
	apnRouter.Get("/school/articleList", (*handler.Apn).FindInfoListHandler)
	apnRouter.Get("/school/articleDetail", (*handler.Apn).GetDetailHandler)
	apnRouter.Get("/add/plan", (*handler.Apn).GetAddPlan)
	apnRouter.Get("/test", (*handler.Apn).GetTest)
	apnRouter.Get("/click", (*handler.Apn).MiddlewareClick)
	apnRouter.Get("/callback", (*handler.Apn).TtCallback)
}
