package api

import (
	"github.com/gocraft/web"
	"addPlan/handler"
	"addPlan/router/api/withdrawal"
)

func ApiRouter(router *web.Router) {
	apiRouter := router.Subrouter(handler.Api{}, "/api")
	apiRouter.Middleware((*handler.Api).ApiSessionMiddleware)

	// 提现业务
	withdrawal.Router(apiRouter)
}
