package withdrawal

import (
	"addPlan/handler"
	"github.com/gocraft/web"
)

func Router(router *web.Router) {
	withdrawal := router.Subrouter(handler.Api{}, "/withdrawal")
	//提现配置
	withdrawal.Post("/confGlobal", (*handler.Api).PostWithdrawalConfGlobal) //提现全局配置-保存
	withdrawal.Get("/confGlobal", (*handler.Api).GetWithdrawalConfGlobal)   //提现全局配置-查看
	withdrawal.Post("/confCash", (*handler.Api).SaveWithdrawalConfCash)     //提现金额配置-保存
	withdrawal.Delete("/confCash", (*handler.Api).DelWithdrawalConfCash)    //提现金额配置-删除
}
