PROJECT=addPlan
BIN_NAME=addPlan
CURRENT_DIR=$(shell pwd)
PUBLISH_DIR=_publish_dir
WORKSPACE=${CURRENT_DIR}

GIT_BIN := $(shell which git 2>/dev/null)

export GO111MODULE=on
export GOPROXY=https://goproxy.cn,direct
export GOSUMDB=off

build:
	echo $(WORKSPACE)
	@echo "compile"
	@rm -rf $(WORKSPACE)/$(PUBLISH_DIR)
	@mkdir -p $(WORKSPACE)/$(PUBLISH_DIR)/$(PROJECT)
	@mkdir -p $(WORKSPACE)/$(PUBLISH_DIR)/$(PROJECT)/bin
	@mkdir -p $(WORKSPACE)/$(PUBLISH_DIR)/$(PROJECT)/conf
	@mkdir -p $(WORKSPACE)/$(PUBLISH_DIR)/$(PROJECT)/log
	@cp -f $(WORKSPACE)/ci/run.sh $(WORKSPACE)/$(PUBLISH_DIR)/$(PROJECT)/bin/run.sh

release: build
	GOOS=linux GOARCH=amd64 go build -v -o $(BIN_NAME)
	mv $(WORKSPACE)/$(BIN_NAME) $(PUBLISH_DIR)/$(PROJECT)/bin
	cp -r $(WORKSPACE)/conf/prod.conf $(PUBLISH_DIR)/$(PROJECT)/conf/prod.conf
