#!/bin/bash

BIN_FILE_ORG=${2:-addPlan}

function stop() {
	supervisorctl stop ${BIN_FILE_ORG}

	if [ $? -ne 0 ]; then
		echo "stop ${BIN_FILE_ORG} fail"
		exit 1
	fi
}

function start() {
	supervisorctl start ${BIN_FILE_ORG}
	if [ $? -ne 0 ]; then
		echo "start ${BIN_FILE_ORG} fail"
		exit 1
	fi
}

function restart() {
	supervisorctl restart ${BIN_FILE_ORG}
	if [ $? -ne 0 ]; then
		echo "start ${BIN_FILE_ORG} fail"
		exit 1
	fi
}

if [ $# -lt 1 ]; then
	echo "usage: $0 [start|stop|restart|test]"
	exit 1
else
	if [ "$1" == 'stop' ] ; then
		stop
		echo "${BIN_FILE_ORG} stop"
	elif [ "$1" == 'start' ] ; then
		start
		echo "${BIN_FILE_ORG} start"
	elif [ "$1" == 'restart' ] ; then
		restart
		echo "${BIN_FILE_ORG} restart"
	else
		echo "usage: $0 [start|stop|restart|test]"
		exit 1
	fi
fi

exit 0
