package handler

import (
	"addPlan/model"
)

type RootContext struct {
	okPacket *model.OkPacket
	err interface{}
}

type Apn struct {
	*RootContext
}

type Web struct {
	*RootContext
}

type Api struct {
	*RootContext
	Role string
	//当前用户
	UserId  uint32
	AccType int

	//模拟登陆用户
	TargetUserId  uint32
	TargetAccType int
}
