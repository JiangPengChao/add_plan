package handler

import (
	log "github.com/alecthomas/log4go"
	"github.com/gocraft/web"
	"runtime"
	"addPlan/utils"
)

func (h *RootContext) HandlePanicMiddleware(w web.ResponseWriter, r *web.Request, next web.NextMiddlewareFunc) {
	defer func() {
		if err := recover(); err != nil {
			logPanicMessage(r, err)

			err = err
			h.okPacket.Code = -2
			h.okPacket.Message = "panic"
			Success(w)
			okPacket, _ := utils.Encode(h.okPacket)
			w.Write(okPacket)
		}
	}()

	next(w, r)
}

func logPanicMessage(r *web.Request, err interface{}) {
	const size = 4096
	stack := make([]byte, size)
	stack = stack[:runtime.Stack(stack, false)]

	data := map[string]interface{}{
		"Error":  err,
		"Stack":  string(stack),
		"Params": r.URL.Query(),
		"Method": r.Method,
	}

	log.Error("HandlePanicMiddleware err message:%v", data)
}
