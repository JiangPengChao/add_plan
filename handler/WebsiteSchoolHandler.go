package handler

import (
	"addPlan/dao"
	"addPlan/model"
	"addPlan/utils"
	"github.com/gocraft/web"
	"strconv"
)



// api 管理员操作
// 新增/修改 文章
func (h *Api) SaveSchoolArticleHandler(rw web.ResponseWriter, req *web.Request) {
	var reqs model.SchoolArticle
	if err := utils.ParseReqBody(req, &reqs); err != nil {
		respWithErr(h, err)
		return
	}

	if len(reqs.ImageUrl) == 0 || len(reqs.Title) == 0 || len(reqs.Content) == 0 {
		respWithErr(h, ParamsIsNull)
		return
	}
	id, err := dao.SchoolArticleServer.SaveSchoolArticle(&reqs)
	if err != nil {
		respWithErr(h, err)
		return
	}

	respWithSuccess(h, id)
	return
}

// 删除
func (h *Api) DelSchoolArticleHandler(rw web.ResponseWriter, req *web.Request) {
	var reqs struct {
		Id int `json:"id"`
	}
	if err := utils.ParseReqBody(req, &reqs); err != nil {
		respWithErr(h, err)
		return
	}
	if reqs.Id <= 0 {
		respWithErr(h, ParamsIsNull)
		return
	}
	if err := dao.SchoolArticleServer.DelSchoolArticle(reqs.Id); err != nil {
		respWithErr(h, err)
		return
	}

	respWithSuccess(h, reqs.Id)
	return
}

//
//func (self *Apn) GetTest(rw web.ResponseWriter, req *web.Request) {
//
//	var urlStr string = "https://www.baidu.com"
//	escapeUrl := url.QueryEscape(urlStr)
//	fmt.Println("编码:",escapeUrl)
//
//	enEscapeUrl, _ := url.QueryUnescape(escapeUrl)
//	fmt.Println("解码:",enEscapeUrl)
//
//	if "2019-08-19" > "2020-08-09" {
//		fmt.Println("111")
//	} else {
//		fmt.Println("333")
//	}
//
//	self.okPacket.Code = 1
//	self.okPacket.Message = "获取成功"
//	self.okPacket.Data = "hello(你好！！！)"
//}



// apn 无登陆状态
// 更新阅读数
func (h *Apn) AddReadNumberHandler(rw web.ResponseWriter, req *web.Request) {
	var reqs struct {
		Id int `json:"id"`
	}
	if err := utils.ParseReqBody(req, &reqs); err != nil {
		respWithErrApn(h, err)
		return
	}
	if reqs.Id <= 0 {
		respWithErrApn(h, ParamsIsNull)
		return
	}

	if err := dao.SchoolArticleServer.AddReadNumber(reqs.Id); err != nil {
		respWithErrApn(h, err)
		return
	}

	respWithSuccessApn(h, reqs.Id)
	return
}

// 获取列表
func (h *Apn) FindInfoListHandler(rw web.ResponseWriter, req *web.Request) {
	articleType, err := strconv.Atoi(utils.ParseReqUrl(req, "article_type"))
	if err != nil {
		respWithErrApn(h, ParamsIsNull)
		return
	}
	if _, ok := model.ArticleTypeMap[model.ArticleType(articleType)]; !ok {
		respWithErrApn(h, ParamsIsNull)
		return
	}

	page, err := strconv.Atoi(utils.ParseReqUrl(req, "page"))
	if err != nil {
		respWithErrApn(h, ParamsIsNull)
		return
	}

	size, err := strconv.Atoi(utils.ParseReqUrl(req, "size"))
	if err != nil {
		respWithErrApn(h, ParamsIsNull)
		return
	}

	title := utils.ParseReqUrl(req, "title")

	dataList, count, err := dao.SchoolArticleServer.FindInfoList(title, model.ArticleType(articleType), page, size)
	if err != nil {
		respWithErrApn(h, err)
		return
	}

	dataListResp := make([]model.SchoolArticleResp, len(dataList))
	for i, data := range dataList {
		resp := model.SchoolArticleResp{}
		resp.ID = data.ID
		resp.Title = data.Title
		resp.Content = data.Content
		resp.ArticleType = data.ArticleType
		resp.ImageUrl = data.ImageUrl
		resp.ReadNumber = data.ReadNumber
		resp.CreatedAt = utils.ParseTimeToString(*data.CreatedAt)
		dataListResp[i] = resp
	}

	var resp = struct {
		DataList interface{} `json:"data_list"`
		Count    int         `json:"count"`
	}{
		DataList: dataListResp,
		Count:    count,
	}

	respWithSuccessApn(h, resp)
	return
}

// 查看详情
func (h *Apn) GetDetailHandler(rw web.ResponseWriter, req *web.Request) {
	id := utils.ParseReqUrl(req, "id")
	if len(id) == 0 {
		respWithErrApn(h, ParamsIsNull)
		return
	}

	data, err := dao.SchoolArticleServer.GetDetail(id)
	if err != nil {
		respWithErrApn(h, err)
		return
	}

	resp := model.SchoolArticleResp{}
	if data != nil {
		resp.ID = data.ID
		resp.Title = data.Title
		resp.Content = data.Content
		resp.ArticleType = data.ArticleType
		resp.ImageUrl = data.ImageUrl
		resp.ReadNumber = data.ReadNumber
		resp.CreatedAt = utils.ParseTimeToString(*data.CreatedAt)
	}

	respWithSuccessApn(h, resp)
	return
}
