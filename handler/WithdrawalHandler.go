package handler

import (
	"addPlan/dao"
	"addPlan/model"
	"addPlan/utils"
	"fmt"
	"github.com/gocraft/web"

	"strconv"
)

/*
提现配置
*/

//提现全局配置-保存
func (h *Api) PostWithdrawalConfGlobal(rw web.ResponseWriter, req *web.Request) {
	var reqs model.WithdrawalConf
	if err := utils.ParseReqBody(req, &reqs); err != nil {
		respWithErr(h, err)
		return
	}
	reqs.Operator = fmt.Sprintf("%d", h.UserId)

	id, err := dao.WithdrawalConfGlobalServer.SaveData(&reqs)
	if err != nil {
		respWithErr(h, err)
		return
	}

	respWithSuccess(h, id)
	return
}

//提现全局配置-查看
func (h *Api) GetWithdrawalConfGlobal(rw web.ResponseWriter, req *web.Request) {
	var resp model.WithdrawalConf
	appId, err := strconv.Atoi(utils.ParseReqUrl(req, "app_id"))
	if err != nil {
		respWithErr(h, ParamsIsNull)
		return
	}

	confGlobal, err := dao.WithdrawalConfGlobalServer.GetDataByAppId(appId)
	if err != nil {
		respWithErr(h, err)
		return
	}
	if confGlobal == nil {
		resp.ConfCashList = make([]model.WithdrawalConfCash, 0)
		respWithSuccess(h, resp)
		return
	}

	confCashList, err := dao.WithdrawalConfCashServer.FindListByConfGlobalId(confGlobal.ID)
	if err != nil {
		respWithErr(h, err)
		return
	}

	resp.Id = confGlobal.ID
	resp.AppId = confGlobal.AppId
	resp.Way = confGlobal.Way
	resp.Explain = confGlobal.Explain
	resp.Operator = confGlobal.Operator
	resp.ConfCashList = confCashList

	respWithSuccess(h, resp)
	return
}

//提现金额配置-保存
func (h *Api) SaveWithdrawalConfCash(rw web.ResponseWriter, req *web.Request) {
	var reqs model.WithdrawalConfCashReq
	if err := utils.ParseReqBody(req, &reqs); err != nil {
		respWithErr(h, err)
		return
	}

	id, err := dao.WithdrawalConfCashServer.SaveData(&reqs)
	if err != nil {
		respWithErr(h, err)
		return
	}

	respWithSuccess(h, id)
	return
}

//提现金额配置-删除
func (h *Api) DelWithdrawalConfCash(rw web.ResponseWriter, req *web.Request) {
	id, err := strconv.Atoi(utils.ParseReqUrl(req, "id"))
	if err != nil {
		respWithErr(h, ParamsIsNull)
		return
	}

	if err := dao.WithdrawalConfCashServer.DelData(id); err != nil {
		respWithErr(h, err)
		return
	}

	respWithSuccess(h, id)
	return
}
