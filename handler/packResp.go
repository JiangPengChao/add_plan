package handler

var (
	ParamsIsNull     = "request params is invalid"
	DataIsNull       = "The data doesn't exist"
	IllegalOperation = "Illegal operation"
)

func respWithErr(api *Api, err interface{}) {
	switch err.(type) {
	case error:
		api.okPacket.Message = err.(error).Error()
	case string:
		api.okPacket.Message = err.(string)
	}
	api.okPacket.Code = 0
}

func respWithSuccess(api *Api, data interface{}) {
	switch data.(type) {
	case string:
		api.okPacket.Message = data.(string)
	default:
		api.okPacket.Message = "Successful"
		api.okPacket.Data = data
	}
	api.okPacket.Code = 1
}

func respWithErrApn(apn *Apn, err interface{}) {
	switch err.(type) {
	case error:
		apn.okPacket.Message = err.(error).Error()
	case string:
		apn.okPacket.Message = err.(string)
	}
	apn.okPacket.Code = 0
}

func respWithSuccessApn(apn *Apn, data interface{}) {
	switch data.(type) {
	case string:
		apn.okPacket.Message = data.(string)
	default:
		apn.okPacket.Message = "Successful"
		apn.okPacket.Data = data
	}
	apn.okPacket.Code = 1
}
