package handler

import (
	"addPlan/share/runtime"
	"addPlan/utils"
	"encoding/json"
	"github.com/gocraft/web"
	"io/ioutil"
	"net/url"
)

func (self *Apn) MiddlewareClick(rw web.ResponseWriter, req *web.Request) {
	runtime.CLog.Debug("上报点击", "上报点击")
	adid := req.URL.Query().Get("adid")
	cid := req.URL.Query().Get("cid")
	imeiMd5 := req.URL.Query().Get("imeiMd5")
	oaid := req.URL.Query().Get("oaid")
	idfa := req.URL.Query().Get("idfa")
	callbackUrl := req.URL.Query().Get("callbackUrl")
	ip := req.URL.Query().Get("ip")
	requestId := req.URL.Query().Get("requestId")
	timestamp := req.URL.Query().Get("timestamp")
	//需要替换自己的域名
    hycallbackurl := "http://cb.hyrainbow.com/callback?=" + callbackUrl
	runtime.CLog.Debug("上报点击", "拼接hy域名生成新的url:%v", hycallbackurl)
    enurl := url.QueryEscape(hycallbackurl)
    url := "https://wcp.taobao.com/adstrack/track.json?action=2&app=1&adAgent=&" +
    	"channel=2200803434433&advertisingSpaceId=501717&taskId=32896&creativeId=226906&" +
    	"adid="+ adid +"&cid="+ cid +"&imeiMd5="+ imeiMd5 +"&oaid="+ oaid +"&idfa="+ idfa +
    	"&callbackUrl="+ enurl +"&ip="+ ip +"&requestId="+ requestId +
    	"&timestamp="+ timestamp +"&appName=toutiao"
	runtime.CLog.Debug("上报点击", "上报点击最终发起url:%v", url)
	header := make(map[string]string)
	params := make(map[string]string)
	resp, err := utils.Get(url, params, header)
	runtime.CLog.Notice("上报点击", "发起请求url:%v", url)
	if err != nil {
		runtime.CLog.Warn("上报点击err", "上报点击错误 err :%v", err)
		self.okPacket.Code = -1
		self.okPacket.Message = "请求失败"
		return
	}
	respData := make(map[string]interface{})
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		runtime.CLog.Warn("上报点击err", "上报点击err ReadAll err :%v", err)
		self.okPacket.Code = -1
		self.okPacket.Message = "读取返回值失败"
		return
	}
	err = json.Unmarshal(body, &respData)
	if err != nil {
		runtime.CLog.Warn("上报点击err", "上报点击err Unmarshal err : %v", err)
		self.okPacket.Code = -1
		self.okPacket.Message = "解析返回值失败"
		return
	}
	runtime.CLog.Warn("上报点击", "上报点击成功")
	self.okPacket.Code = 1
	self.okPacket.Message = "上报成功"
}


func (self *Apn) TtCallback(rw web.ResponseWriter, req *web.Request) {
	runtime.CLog.Debug("淘宝转化回调", "回调头条")
	callback := req.URL.Query().Get("callback")
	callback_url, _ := url.QueryUnescape(callback)
	runtime.CLog.Debug("淘宝转化回调", "回调url:%v", callback_url)
	header := make(map[string]string)
	params := make(map[string]string)
	resp, err := utils.Get(callback_url, params, header)
	runtime.CLog.Notice("淘宝转化回调", "发起请求头条url：", callback_url)
	if err != nil {
		runtime.CLog.Warn("淘宝转化回调", "请求错误 err :%v", err)
		self.okPacket.Code = -1
		self.okPacket.Message = "请求失败"
		return
	}
	respData := make(map[string]interface{})
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		runtime.CLog.Warn("淘宝转化回调", "Err ReadAll err :%v", err)
		self.okPacket.Code = -1
		self.okPacket.Message = "读取返回值失败"
		return
	}
	err = json.Unmarshal(body, &respData)
	if err != nil {
		runtime.CLog.Warn("淘宝转化回调", "Err Unmarshal err : %v", err)
		self.okPacket.Code = -1
		self.okPacket.Message = "解析返回值失败"
		return
	}
	runtime.CLog.Warn("淘宝转化回调", "回调成功")
	self.okPacket.Code = 1
	self.okPacket.Message = "回调成功"
}
