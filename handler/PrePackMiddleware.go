package handler

import (
	"github.com/gocraft/web"
	"addPlan/model"
	"addPlan/utils"
)

func (h *RootContext) PrePackMiddleware(w web.ResponseWriter, r *web.Request, next web.NextMiddlewareFunc) {
	//TODO: Use this function to send response automatically
	defer func() {
		if h.err == nil {
			okPacket, _ := utils.Encode(h.okPacket)
			w.Write(okPacket)
		}
	}()

	h.okPacket = &model.OkPacket{}
	next(w, r)
}
