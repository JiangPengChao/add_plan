package handler

import (
	"addPlan/dao"
	"addPlan/model"
	"addPlan/share/runtime"
	"addPlan/utils"
	"github.com/gocraft/web"
	"io/ioutil"
	"strings"
	"time"
)

//创建计划脚本每条早晨6点半执行该脚本
func AddPlan() {
	var err error
	now := utils.GetCurrentDate()
	//details 用户信息 M1 T0
	detailsM1, count_M1, err := dao.GetUserDetails(now, 6, "TEST-001")
	runtime.CLog.Debug("获取用户数据", "今日用户M1数据总数:%v", count_M1)
	if err != nil {
		runtime.CLog.Warn("获取用户数据", "err :%v", err)
		dao.SendDingTalkMessage("服务名称：定时创建计划，定时脚本警告️内容：获取用户数据错误M1")
		return
	}
	var detailsT0 []*model.ParamsDetail
	tagArr := dao.GetT0TagArr()
	for _, j := range tagArr {
		details_zb, _, err := dao.GetUserDetails(now, 6, j)
		if err != nil {
			runtime.CLog.Warn("获取用户数据", "err :%v", err)
			dao.SendDingTalkMessage("服务名称：定时创建计划，定时脚本警告️内容：获取用户数据错误T0")
			return
		}
		detailsT0 = append(detailsT0, details_zb...)
	}
	count_T0 := len(detailsT0)
	runtime.CLog.Debug("获取用户数据", "今日用户T0数据总数:%v", count_T0)
	//sipname 线路名称
	sipname := "duke_sti_001"
	month := time.Now().Format("01")
	day := time.Now().Format("02")
	plannames := dao.GetPlanNameArr()
	for i, j := range plannames {
		var details []*model.ParamsDetail
		if i < 7 {
			details = detailsT0
		} else {
			details = detailsM1
		}
		//planname 计划名称
		planname := strings.Replace(j, "0426", month+day, -1)
		print(planname, "---")
		//strategyname 策略名称
		strategyname, _ := dao.GetStrategyNameByTime(planname[len(planname)-4:])
		header := make(map[string]string)
		params := make(map[string]interface{})
		params["Strategy"] = strategyname
		params["SIPLine"] = sipname
		params["Details"] = details
		params["PlanName"] = planname
		url := "http://cmp.hicall.ai/s2s/v1/plan"
		runtime.CLog.Notice("请求创建计划接口", "add plan api post start url: %v, params is: %v", url, params)
		resp, err := utils.Post(url, params, nil, header)
		if err != nil {
			runtime.CLog.Warn("请求创建计划接口", "add plan api post err: %v, resp is: %v", err, resp)
			dao.SendDingTalkMessage("服务名称：定时创建计划，定时脚本警告️内容：add plan api post err")
			return
		}
		body, err := ioutil.ReadAll(resp.Body)
		println(string(body))
		runtime.CLog.Debug("请求创建计划接口", "Callback WECHAT_COMPONENT_TOKEN body: %v", string(body))
		if err != nil {
			runtime.CLog.Warn("请求创建计划接口", "add plan api  ReadAll err :%v", err)
			dao.SendDingTalkMessage("服务名称：定时创建计划，定时脚本警告️内容：add plan api post err")
			return
		}
	}
}

func (self *Apn) GetAddPlan(rw web.ResponseWriter, req *web.Request) {
	var err error
	hour := time.Now().Hour() - 1
	minute := time.Now().Minute()
	now := utils.GetCurrentDate()
	//details 用户信息 M1 T0
	detailsM1, count, err := dao.GetUserDetails(now, hour, "TEST-001")
	runtime.CLog.Debug("获取用户数据", "HTTP今日用户M1数据总数:%v, M1数据:%v", count, detailsM1)
	if err != nil {
		runtime.CLog.Warn("获取用户数据", "err :%v", err)
		dao.SendDingTalkMessage("服务名称：定时创建计划，定时脚本警告️内容：获取用户数据错误M1")
		return
	}
	var detailsT0 []*model.ParamsDetail
	tagArr := dao.GetT0TagArr()
	for _, j := range tagArr {
		details_zb, _, err := dao.GetUserDetails(now, 6, j)
		if err != nil {
			runtime.CLog.Warn("获取用户数据", "err :%v", err)
			dao.SendDingTalkMessage("服务名称：定时创建计划，定时脚本警告️内容：获取用户数据错误T0")
			return
		}
		detailsT0 = append(detailsT0, details_zb...)
	}
	count_T0 := len(detailsT0)
	runtime.CLog.Debug("获取用户数据", "HTTP今日用户T0数据总数:%v", count_T0)
	//sipname 线路名称
	sipname := req.URL.Query().Get("sipline")
	if len(sipname) == 0 {
		sipname = "duke_sti_001"
	}
	month := time.Now().Format("01")
	day := time.Now().Format("02")
	plannames := dao.GetPlanNameArr()
	var newPlan []string
	hour = hour*100 + minute
	for _, j := range plannames {
		_, timecount := dao.GetStrategyNameByTime(j[len(j)-4:])
		if timecount > hour {
			newPlan = append(newPlan, j)
		}
	}

	for _, j := range newPlan {
		var details []*model.ParamsDetail
		prefix := j[:2]
		if prefix == "T0" {
			details = detailsT0
		}
		if prefix == "M1" {
			details = detailsM1
		}
		//planname 计划名称
		planname := strings.Replace(j, "0426", month+day, -1)
		print(planname, "---")
		//strategyname 策略名称
		strategyname, _ := dao.GetStrategyNameByTime(planname[len(planname)-4:])
		header := make(map[string]string)
		params := make(map[string]interface{})
		params["Strategy"] = strategyname
		params["SIPLine"] = sipname
		params["Details"] = details
		params["PlanName"] = planname
		url := "http://cmp.hicall.ai/s2s/v1/plan"
		runtime.CLog.Notice("请求创建计划接口", "HTTP add plan api post start url: %v, params is: %v", url, params)
		resp, err := utils.Post(url, params, nil, header)
		if err != nil {
			runtime.CLog.Warn("请求创建计划接口", "HTTP add plan api post err: %v, resp is: %v", err, resp)
			dao.SendDingTalkMessage("服务名称：定时创建计划，HTTP警告️内容：add plan api post err")
			return
		}
		body, err := ioutil.ReadAll(resp.Body)
		println(string(body))
		runtime.CLog.Debug("请求创建计划接口", "HTTP add plan api body: %v", string(body))
		if err != nil {
			runtime.CLog.Warn("请求创建计划接口", "HTTP add plan api  ReadAll err :%v", err)
			dao.SendDingTalkMessage("服务名称：定时创建计划，HTTP警告️内容：add plan api  ReadAll err")
			return
		}
	}

	self.okPacket.Code = 1
	self.okPacket.Message = "计划任务手动创建成功"
}

func (self *Apn) GetTest(rw web.ResponseWriter, req *web.Request) {
	var err error
	now := utils.GetCurrentDate()
	//details 用户信息 M1 T0
	detailsM1, count, err := dao.GetUserDetails(now, 6, "TEST-001")
	runtime.CLog.Debug("获取用户数据", "今日用户M1数据总数:%v, M1数据:%v", count, detailsM1)
	if err != nil {
		runtime.CLog.Warn("获取用户数据", "err :%v", err)
		dao.SendDingTalkMessage("服务名称：定时创建计划，定时脚本警告️内容：获取用户数据错误M1")
		return
	}

	dao.SendDingTalkMessage("服务名称：定时创建计划，定时脚本警告️内容：你好，测试！！！")

	sipline := req.URL.Query().Get("sipline")
	if len(sipline) == 0 {
		sipline = "duke"
	}
	println(sipline)

	a := "0899999976236"
	println(a[1:])

	self.okPacket.Code = 1
	self.okPacket.Message = "获取成功"
	self.okPacket.Data = "data"
}

//duke YX
func AddPromotionTask() {
	runtime.CLog.Debug("创建营销任务", "开始创建营销任务")
	var err error
	now := utils.GetCurrentDate()
	//details 用户信息 M1 T0
	pDetails, pCount, err := dao.GetUserPromotionDetails(now, 6, "KF0001", "KF0002")
	runtime.CLog.Debug("获取用户数据", "今日营销任务用户数据总数:%v", pCount)
	if err != nil {
		runtime.CLog.Warn("获取用户数据", "err :%v", err)
		dao.SendDingTalkMessage("服务名称：定时创建计划，定时脚本警告️内容：获取用户数据错误M1")
		return
	}
	month := time.Now().Format("01")
	day := time.Now().Format("02")
	//获取任务名称、任务计划开始时间、任务计划结束时间
	repeat_interval := 10
	repeat_number := 6
	robot_id := "1b688a6c26104e84bcf95a8ea9b877d4"
	sip_line := "duke_sti_001"
	details := pDetails
	company := "duke"

	task_name, schedule_start_time, schedule_end_time := dao.GetTaskInfo(company, 0)
	task_name = strings.Replace(task_name, "0524", month+day, -1)
	schedule_start_time = now + schedule_start_time
	schedule_end_time = now + schedule_end_time
	header := make(map[string]string)
	params := make(map[string]interface{})
	params["RepeatInterval"] = repeat_interval
	params["RepeatNumber"] = repeat_number
	params["RobotID"] = robot_id
	params["SIPLine"] = sip_line
	params["Details"] = details
	params["TaskName"] = task_name
	params["ScheduleStartTime"] = schedule_start_time
	params["ScheduleEndTime"] = schedule_end_time
	params["operator"] = "dukeYX"
	params["company"] = company
	url := "http://cmp.hicall.ai/s2s/v1/auto/create"
	resp, err := utils.Post(url, params, nil, header)
	if err != nil {
		runtime.CLog.Warn("请求创建营销任务接口", "add promotion task api post err: %v, resp is: %v", err, resp)
		dao.SendDingTalkMessage("服务名称：定时创建营销任务，定时脚本警告️内容：add promotion task api err")
		return
	}
	body, err := ioutil.ReadAll(resp.Body)
	println(string(body))
	runtime.CLog.Debug("请求创建营销任务接口", "add promotion task api body: %v", string(body))
	if err != nil {
		runtime.CLog.Warn("请求创建营销任务接口", "add promotion task api :%v", err)
		dao.SendDingTalkMessage("服务名称：定时创建计划，定时脚本警告️内容：add promotion task api err")
		return
	}

}

//Ruidao YX
func AddPromotionTaskRuidao() {
	runtime.CLog.Debug("创建营销任务", "开始创建营销任务")
	var err error
	now := utils.GetCurrentDate()
	//details 用户信息 M1 T0
	pDetails, pCount, err := dao.GetUserPromotionDetailsRuidao(now)
	runtime.CLog.Debug("获取用户数据", "Ruidao今日营销任务用户数据总数:%v", pCount)
	if err != nil {
		runtime.CLog.Warn("获取用户数据", "err :%v", err)
		dao.SendDingTalkMessage("服务名称：Ruidao定时创建计划，定时脚本警告️内容：获取用户数据错误")
		return
	}
	month := time.Now().Format("01")
	day := time.Now().Format("02")
	//获取任务名称、任务计划开始时间、任务计划结束时间
	repeat_interval := 5
	repeat_number := 7
	robot_id := "e313ccb12d4d0b8ab8217b4492e114c1"
	sip_line := "ruidao_id_sti"
	details := pDetails
	company := "Ruidao"

	for i := 1; i < 3; i++ {
		task_name, schedule_start_time, schedule_end_time := dao.GetTaskInfo(company, i)
		task_name = strings.Replace(task_name, "0524", month+day, -1)
		schedule_start_time = now + schedule_start_time
		schedule_end_time = now + schedule_end_time
		header := make(map[string]string)
		params := make(map[string]interface{})
		params["RepeatInterval"] = repeat_interval
		params["RepeatNumber"] = repeat_number
		params["RobotID"] = robot_id
		params["SIPLine"] = sip_line
		params["Details"] = details
		params["TaskName"] = task_name
		params["ScheduleStartTime"] = schedule_start_time
		params["ScheduleEndTime"] = schedule_end_time
		params["operator"] = "RuidaoYX"
		params["company"] = company
		url := "http://cmp.hicall.ai/s2s/v1/auto/create"
		resp, err := utils.Post(url, params, nil, header)
		if err != nil {
			runtime.CLog.Warn("请求创建营销任务接口", "Ruidao add promotion task api post err: %v, resp is: %v", err, resp)
			dao.SendDingTalkMessage("服务名称：Ruidao定时创建营销任务，定时脚本警告️内容：add promotion task api err")
			return
		}
		body, err := ioutil.ReadAll(resp.Body)
		println(string(body))
		runtime.CLog.Debug("请求创建营销任务接口", "Ruidao add promotion task api body: %v", string(body))
		if err != nil {
			runtime.CLog.Warn("请求创建营销任务接口", "Ruidao add promotion task api :%v", err)
			dao.SendDingTalkMessage("服务名称：Ruidao定时创建计划，定时脚本警告️内容：add promotion task api err")
			return
		}
	}
}
