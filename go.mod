module addPlan

go 1.13

require (
	bitbucket.org/hyrainbow/clog v1.0.1
	github.com/alecthomas/log4go v0.0.0-20180109082532-d146e6b86faa
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gocraft/web v0.0.0-20190207150652-9707327fb69b
	github.com/jinzhu/gorm v1.9.16
	github.com/json-iterator/go v1.1.10
	github.com/onsi/ginkgo v1.14.2 // indirect
	github.com/onsi/gomega v1.10.3
	github.com/robfig/cron v1.2.0
	github.com/tealeg/xlsx v1.0.5
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897
	gopkg.in/gcfg.v1 v1.2.3
	gopkg.in/warnings.v0 v0.1.2 // indirect

)
