package utils

import (
	"github.com/gocraft/web"
	"io/ioutil"
)

func ParseReqBody(req *web.Request, v interface{}) error {
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return err
	}

	return Decode(body, v)
}

func ParseReqUrl(req *web.Request, name string) string {
	return req.URL.Query().Get(name)
}
