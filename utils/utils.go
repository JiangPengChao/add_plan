package utils

import (
	"addPlan/share/runtime"
	"addPlan/utils/strconvx"
	"bytes"
	"crypto/md5"
	"encoding/hex"
	"errors"
	"fmt"
	"github.com/gocraft/web"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"
)

const (
	IMGPNG  = "image/png"
	IMGJPEG = "image/jpeg"
	APK     = "application/vnd.android.package-archive"
)

func Get(url string, params map[string]string, headers map[string]string) (*http.Response, error) {
	//new request
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		runtime.CLog.Warn("HTTP GET", "NewRequest err: %v", err)
		return nil, errors.New("new request is fail ")
	}

	//add params
	q := req.URL.Query()
	if params != nil {
		for key, val := range params {
			q.Add(key, val)
		}
		req.URL.RawQuery = q.Encode()
	}

	//add headers
	if headers != nil {
		for key, val := range headers {
			req.Header.Add(key, val)
		}
	}

	//http client
	client := &http.Client{Timeout: time.Second * 60}
	runtime.CLog.Warn("HTTP Get", "Go %s URL : %s", http.MethodGet, req.URL.String())
	return client.Do(req)
}

func Post(url string, body interface{}, params map[string]string, headers map[string]string) (*http.Response, error) {
	//add post body
	var bodyJson []byte
	var req *http.Request
	if body != nil {
		var err error
		bodyJson, err = json.Marshal(body)
		if err != nil {
			//runtime.CLog.Warn("HTTP POST", "Marshal err: %v", err)
			return nil, errors.New("http post body to json failed")
		}
	}

	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(bodyJson))
	if err != nil {
		//runtime.CLog.Warn("HTTP POST", "new request err: %v", err)
		return nil, errors.New("new request is fail: %v \n")
	}
	req.Header.Set("Content-type", "application/json")

	//add params
	q := req.URL.Query()
	if params != nil {
		for key, val := range params {
			q.Add(key, val)
		}
		req.URL.RawQuery = q.Encode()
	}

	//add headers
	if headers != nil {
		for key, val := range headers {
			req.Header.Add(key, val)
		}
	}

	//http client
	client := &http.Client{Timeout: time.Second * 5}
	//runtime.CLog.Warn("HTTP POST", "Go %s URL : %s", http.MethodPost, req.URL.String())
	return client.Do(req)
}

//模拟登陆
func TargetLogin(req *web.Request, uid uint32) uint32 {
	userId := req.URL.Query().Get("userID")
	if len(userId) == 0 {
		return uid
	} else {
		return strconvx.AToUint32(userId)
	}
}

//ak传8，as传32
func GetRandomString(l int) string {
	str := "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	bytes := []byte(str)
	result := []byte{}
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := 0; i < l; i++ {
		result = append(result, bytes[r.Intn(len(bytes))])
	}
	return string(result)
}

func Code() string {
	rand.Seed(time.Now().UnixNano())
	code := rand.Intn(899999) + 100000
	s := strconv.Itoa(code)
	return s
}

//检测变量类型
func typeof(v interface{}) string {
	return fmt.Sprintf("%T", v)
}

func MD5(str string) string {
	s := md5.New()
	s.Write([]byte(str))
	return hex.EncodeToString(s.Sum(nil))
}

// 20200908-20200909  ===>  2020-09-08 and 2020-09-09
func GetDataFromURLQuery(req *web.Request, name string) (startTime, endTime string) {
	var (
		timeLayout  = "20060102"
		timeFormate = "2006-01-02"
	)
	dataList := strings.Split(req.URL.Query().Get(name), "-")
	if len(dataList) < 2 {
		log.Println("dataList is null")
		return
	}

	t1, err := time.Parse(timeLayout, dataList[0])
	if err != nil {
		log.Println("time.Parse err", err.Error())
		return
	}
	startTime = t1.Format(timeFormate)

	t2, err := time.Parse(timeLayout, dataList[1])
	if err != nil {
		log.Println("time.Parse err", err.Error())
		return
	}
	endTime = t2.Format(timeFormate)
	return
}

// 2006-01-02 15:04:05
func ParseStringToTime(data string) (t time.Time) {
	var (
		timeFormate = "2006-01-02"
		err         error
	)
	t, err = time.Parse(timeFormate, data)
	if err != nil {
		log.Println("time.Parse err", err.Error())
		return
	}
	return
}

// 2006-01-02
func ParseTimeToString(data time.Time) (t string) {
	t = data.Format("2006-01-02")
	return
}

// 2006-01-02 15:04:05
func ParseTimeToString2(data time.Time) (t string) {
	t = data.Format("2006-01-02 15:04:05")
	return
}

// nginx反向代理下，golang程序获取用户真实IP
// https://studygolang.com/articles/15988?fr=sidebar
// https://www.cnblogs.com/binghe001/p/13279626.html
func GetClientIP(req *web.Request) string {
	ip := req.Request.Header.Get("X-Forwarded-For")
	if strings.Contains(ip, "127.0.0.1") || ip == "" {
		ip = req.Request.Header.Get("X-real-ip")
	}

	if ip == "" {
		return "127.0.0.1"
	}

	return ip
}

func GetCurrentDateTime() string {
	now := time.Now().Format("2006-01-02 15:04:05")
	return now
}

func GetCurrentDate() string {
	now := time.Now().Format("2006-01-02")
	return now
}