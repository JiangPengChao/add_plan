package share

import (
	"addPlan/model"
	"database/sql"
	"errors"
	log "github.com/alecthomas/log4go"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"strings"
)

var (
	Env                   string
	err                   error
	YunHaiDBGorm          *gorm.DB
	AddPlan               *sql.DB
	GormErrRecordNotFound = gorm.ErrRecordNotFound
	GormNoRowsInResultSet = errors.New("sql: no rows in result set")
)

func InitDB(context *model.GlobalContext) {
	log.Debug("create mysql")
	Env = context.Server.Env

	if AddPlan, err = sql.Open("mysql", context.AddPlan.Address); err != nil {
		log.Debug("MySQL Adv connection err:", err)
		panic(err)
	}
	AddPlan.SetMaxIdleConns(10)

	YunHaiDBGorm, err = gorm.Open("mysql", context.AddPlan.Address)
	if err != nil {
		print("err", err)
	}
	if strings.ToLower(*RunEnv) == "debug" {
		YunHaiDBGorm.LogMode(true)
	}

	YunHaiDBGorm.DB().SetMaxOpenConns(context.AddPlan.MaxConn)
	YunHaiDBGorm.SingularTable(true)
	autoMigrate(YunHaiDBGorm)
}

// 自动构建表结构
func autoMigrate(db *gorm.DB) {
	db.AutoMigrate(&model.SchoolArticle{})
}
