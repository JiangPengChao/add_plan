package share

import (
	"addPlan/model"
	"addPlan/share/runtime"
	"bitbucket.org/hyrainbow/clog"
	"flag"
	log "github.com/alecthomas/log4go"
	"gopkg.in/gcfg.v1"
)

var (
	GlobalContext model.GlobalContext
	configFile    = flag.String("c", "conf/prod.conf", "config file path")
	RunEnv        = flag.String("env", "prod", "Running env")
)

func InitConf(file string) {
	//读取配置文件
	if len(file) != 0 {
		*configFile = file
	} else {
		flag.Parse()
	}

	log.Debug("加载的配置文件: %v", *configFile)
	if err := gcfg.ReadFileInto(&GlobalContext, *configFile); err != nil {
		log.Debug("Init config err: %v", err)
		return
	}

	clogConf := &clog.Conf{
		Name:    GlobalContext.Log.Name,
		LogPath: GlobalContext.Log.LogPath,
		BufSize: map[clog.LogLevel]int{
			clog.NoticeLevel:  GlobalContext.Log.BufSize,
			clog.FatalLevel:   GlobalContext.Log.BufSize,
			clog.WarningLevel: GlobalContext.Log.BufSize,
			clog.DebugLevel:   GlobalContext.Log.BufSize},
		MaxEnableLevel: clog.LogLevel(GlobalContext.Log.MaxEnableLevel),
		NeedEmail:      GlobalContext.Log.NeedEmail,
		EmailConf: &clog.EmailConf{
			Server:    GlobalContext.Email.Server,
			Sender:    GlobalContext.Email.Sender,
			Password:  GlobalContext.Email.Password,
			Receivers: GlobalContext.Email.Receivers,
			Subject:   GlobalContext.Email.Subject,
		},
	}

	var err error
	runtime.CLog, err = clog.NewClog(clogConf)
	if err != nil {
		log.Debug("Init CLog err: %v", err)
	}
}
