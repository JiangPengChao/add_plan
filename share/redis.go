package share

import (
	"addPlan/model"
	"fmt"
	log "github.com/alecthomas/log4go"
	goRedis "github.com/go-redis/redis"
)

var (
	GoRedis *goRedis.Client
	err1    error
)

func InitRedis(context *model.GlobalContext) {
	log.Debug("create redis")
	GoRedis = goRedis.NewClient(&goRedis.Options{
		Addr:     context.Redis.Address,  //主机
		Password: context.Redis.Password, //密码
	})
	_, err := GoRedis.Ping().Result() //ping连接
	if err != nil {
		log.Debug("Redis", "connection err: %v", err1)
		panic(fmt.Sprint("create redis error:", err1))
	}
}
