package main

import (
	"addPlan/handler"
	routers "addPlan/router"
	"addPlan/share"
	"github.com/robfig/cron"
	"log"
	"net/http"
	"os"
	"time"
)

func init() {
	//初始化配置参数及环境变量
	share.InitConf("")
	//初始化mysql
	share.InitDB(&share.GlobalContext)
	////初始化redis
	//share.InitRedis(&share.GlobalContext)
	//// 初始化Server
	//dao.InitServer()
}

func main() {
	rootRouter := routers.InitRouter()
	ch := make(chan error, 1)
	// pprof
	go func() {
		// 启动一个 http server，注意 pprof 相关的 handler 已经自动注册过了，和正常提供业务服务的端口不同
		if len(share.GlobalContext.Server.Pporf) > 0 {
			log.Printf("Start Http Pprof Server: [%s]\n", share.GlobalContext.Server.Pporf)
			if err := http.ListenAndServe(share.GlobalContext.Server.Pporf, nil); err != nil {
				log.Fatal(err)
			}
			os.Exit(0)
		}
	}()

	go func() {
		log.Printf("Start Http Server: [%s]\n", share.GlobalContext.Server.Port)
		ch <- http.ListenAndServe(share.GlobalContext.Server.Port, rootRouter)
	}()

	go func() {
		c := cron.New()
		spec := "0 30 6 * * ?"
		spec_1 := "0 59 11 * * ?"
		c.AddFunc(spec, func() {   // 每条六点半跑计划
			log.Println("cron running create plan")
			handler.AddPlan()
			handler.AddPromotionTask()
		})
		c.AddFunc(spec_1, func() {   // 每条11点59跑计划
			log.Println("cron running create plan 2")
			handler.AddPromotionTaskRuidao()
		})
		c.Start()

		select{}
	}()

	err := <-ch
	log.Printf("Sever err: %v", err)

	<-time.After(10 * time.Second)
}
