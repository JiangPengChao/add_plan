package dao

import (

)

var (
	SchoolArticleServer        *SchoolArticlesDao
	WithdrawalConfGlobalServer *ConfGlobalDao
	WithdrawalConfCashServer   *ConfCashDao
)

func InitServer() {
	SchoolArticleServer = NewSchoolArticleDao()
	WithdrawalConfGlobalServer = NewConfGlobalDao()
	WithdrawalConfCashServer = NewConfCashDao()
}
