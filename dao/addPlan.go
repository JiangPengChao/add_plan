package dao

import (
	"addPlan/model"
	"addPlan/share"
	"addPlan/share/runtime"
	"addPlan/utils"
	"strconv"
)

const (
	DATE                                  = "2006-01-02"
	SELECT_CURRENT_DATE_CS_DATA           = "SELECT id, date, hour, phone, applyTime, repaymentTime, name, amount, discName, account FROM cs_data WHERE date=? and hour=? and account=?"
	SELECT_PDetail_CURRENT_DATE_CS_DATA   = "SELECT id, date, hour, phone, discName, account, gender, name FROM cs_data WHERE date=? and hour=? and (account=? or account =?)"
	SELECT_PDetail_CURRENT_DATE_DUKE_LOAN = "SELECT id, date, phone, name, app, product FROM duke_loan WHERE date=? "
)

func GetUserDetails(now string, hour int, tag string) ([]*model.ParamsDetail, int, error) {
	ret, err := share.AddPlan.Query(SELECT_CURRENT_DATE_CS_DATA, now, hour, tag)
	defer ret.Close()
	if err != nil {
		return nil, 0, err
	}
	csData := make([]*model.CsData, 0)
	for ret.Next() {
		item := new(model.CsData)
		ret.Scan(&item.Id, &item.DateAt, &item.Hour, &item.Phone, &item.ApplyTimeAt, &item.RepaymentTimeAt, &item.Name, &item.Amount, &item.DiscName, &item.Account)
		item.Date = item.DateAt.Format((DATE))
		item.ApplyTime = item.ApplyTimeAt.Format((DATE))
		item.RepaymentTime = item.RepaymentTimeAt.Format((DATE))
		csData = append(csData, item)
	}
	var error string
	//处理数据
	returnDate := make([]*model.ParamsDetail, 0)
	for i, j := range csData {
		item := new(model.ParamsDetail)
		item.Debtor = j.Name
		item.Mobile = "+62" + j.Phone
		item.LoanDate = j.ApplyTime
		item.DueDate = j.RepaymentTime
		amount, _ := strconv.ParseFloat(j.Amount, 64)
		item.LoanAmount = amount
		item.Arrears = 20310.234
		item.Unit = "USD"
		item.Platform, error = GetPlatformByDiscname(j.DiscName, i)
		if error != "" {
			continue
		}
		item.ExtraA = j.RepaymentTime[6:]
		returnDate = append(returnDate, item)
	}

	return returnDate, len(csData), nil
}

func GetPlatformByDiscname(discname string, i int) (string, string) {
	var platform string
	switch discname {
	case "A盘-KAMIGO":
		platform = "KAMIGO"
	case "B盘-DANASAKU":
		platform = "DANASAKU"
	case "C盘-nyilehduit.dana":
		platform = "NYILEHDUIT"
	case "D盘-com.danarupiah.tunaicepat.pinjamanuang.cash":
		platform = "PINJAMANAJA"
	case "Kreditpedia":
		platform = "KREDITPEDIA"
	case "OCM":
		platform = "OCM"
	case "estrend":
		platform = "ESTREND"
	default:
		runtime.CLog.Debug("获取用户数据", "该discname:%v 不在范围内,i=:%v", discname, i)
		a := i % 3
		if a == 0 {
			//发出报警
			SendDingTalkMessage("此discname：" + discname + "无映射关系")
		}
		return "", "无对应app"
	}
	return platform, ""
}

func GetPlanNameArr() [14]string {
	var PlanName [14]string
	PlanName[0] = "T0_ID_sti_round1_0426_api_0800"
	PlanName[1] = "T0_ID_sti_round1_0426_api_0930"
	PlanName[2] = "T0_ID_sti_round1_0426_api_1100"
	PlanName[3] = "T0_ID_sti_round1_0426_api_1230"
	PlanName[4] = "T0_ID_sti_round1_0426_api_1430"
	PlanName[5] = "T0_ID_sti_round1_0426_api_1630"
	PlanName[6] = "T0_ID_sti_round1_0426_api_1900"
	PlanName[7] = "M1_ID_sti_round1_0426_api_0800"
	PlanName[8] = "M1_ID_sti_round1_0426_api_0930"
	PlanName[9] = "M1_ID_sti_round1_0426_api_1100"
	PlanName[10] = "M1_ID_sti_round1_0426_api_1230"
	PlanName[11] = "M1_ID_sti_round1_0426_api_1430"
	PlanName[12] = "M1_ID_sti_round1_0426_api_1630"
	PlanName[13] = "M1_ID_sti_round1_0426_api_1900"
	return PlanName
}

func GetT0TagArr() [2]string {
	var tag [2]string
	tag[0] = "TEST-101"
	tag[1] = "RB-001"
	return tag
}

func GetStrategyNameByTime(sj string) (string, int) {
	var strategyName string
	var timecount int
	switch sj {
	case "0800":
		strategyName = "8点拨打"
		timecount = 800
	case "0930":
		strategyName = "9点30拨打"
		timecount = 930
	case "1100":
		strategyName = "11点拨打"
		timecount = 1100
	case "1230":
		strategyName = "12点30拨打"
		timecount = 1230
	case "1430":
		strategyName = "14点30拨打"
		timecount = 1430
	case "1630":
		strategyName = "16点30拨打"
		timecount = 1630
	case "1900":
		strategyName = "19点拨打"
		timecount = 1900
	}
	return strategyName, timecount

}

func SendDingTalkMessage(text string) {
	url := "http://172.16.1.7:8999/sendDingTalk"
	header := make(map[string]string)
	params := make(map[string]interface{})
	params["message"] = "服务名称：定时创建计划，警告️内容：" + text
	resp, err := utils.Post(url, params, nil, header)
	if err != nil {
		runtime.CLog.Warn("send_dingTalk_message", "send_dingTalk_message err: %v, resp is: %v", err, resp)
		return
	}
}

func GetUserPromotionDetails(now string, hour int, tag, tag1 string) ([]*model.ParamsPDetail, int, error) {
	ret, err := share.AddPlan.Query(SELECT_PDetail_CURRENT_DATE_CS_DATA, now, hour, tag, tag1)
	defer ret.Close()
	if err != nil {
		return nil, 0, err
	}
	pCsData := make([]*model.PCsData, 0)
	for ret.Next() {
		item := new(model.PCsData)
		ret.Scan(&item.Id, &item.DateAt, &item.Hour, &item.Phone, &item.DiscName, &item.Account, &item.Gender, &item.Name)
		item.Date = item.DateAt.Format((DATE))
		pCsData = append(pCsData, item)
	}
	var error string
	//处理数据
	returnDate := make([]*model.ParamsPDetail, 0)
	for i, j := range pCsData {
		item := new(model.ParamsPDetail)
		item.Customer = j.Name
		item.Mobile = "+62" + j.Phone
		item.Platform, error = GetPPlatformByDiscname(j.DiscName, i)
		item.Gender = GetUserGender(j.Gender)
		if error != "" {
			continue
		}
		returnDate = append(returnDate, item)
	}

	return returnDate, len(pCsData), nil
}

func GetUserPromotionDetailsRuidao(now string) ([]*model.ParamsPDetail, int, error) {
	ret, err := share.AddPlan.Query(SELECT_PDetail_CURRENT_DATE_DUKE_LOAN, now)
	defer ret.Close()
	if err != nil {
		return nil, 0, err
	}
	pDukeData := make([]*model.PDukeLoan, 0)
	for ret.Next() {
		item := new(model.PDukeLoan)
		ret.Scan(&item.Id, &item.DateAt, &item.Phone, &item.Name, &item.App, &item.Product)
		item.Date = item.DateAt.Format((DATE))
		pDukeData = append(pDukeData, item)
	}
	var error string
	var phone string
	//处理数据
	returnDate := make([]*model.ParamsPDetail, 0)
	for i, j := range pDukeData {
		item := new(model.ParamsPDetail)
		item.Customer = j.Name
		phone = j.Phone
		item.Mobile = "+62" + phone[1:]
		item.Platform, error = GetPPlatformByDiscname(j.App+"-"+j.Product, i)
		if error != "" {
			continue
		}
		returnDate = append(returnDate, item)
	}

	return returnDate, len(pDukeData), nil
}

func GetUserGender(gender string) string {
	var g string
	switch gender {
	case "LAKI-LAKI":
		g = "male"
	case "PEREMPUAN":
		g = "female"
	default:
		g = ""
	}
	return g
}

func GetPPlatformByDiscname(discname string, i int) (string, string) {
	var platform string
	switch discname {
	case "KAMIGO":
		platform = "KAMIGO"
	case "DANASAKU":
		platform = "DANASAKU"
	case "nyilehduit.dana":
		platform = "NYILEHDUIT"
	case "com.danarupiah.tunaicepat.pinjamanuang.cash":
		platform = "PINJAMANAJA"
	case "KSP Pinjaman Uang":
		platform = "PINJAMANAJA"
	case "KSP Dompet Dana":
		platform = "DANASAKU"
	case "Tunai Bijak-KSP Pinjaman Uang":
		platform = "DFPINJAMANAJA"
	case "Uang Ku-KSP Pinjaman Uang":
		platform = "DFPINJAMANAJA"
	case "Money Go-KSP Pinjaman Uang":
		platform = "DFPINJAMANAJA"
	case "KTA Bijak-KSP Pinjaman Uang":
		platform = "DFPINJAMANAJA"
	case "Tunai Pinjaman-KSP Pinjaman Uang":
		platform = "DFPINJAMANAJA"
	case "Dompet Pintar-KSP Pinjaman Uang":
		platform = "DFPINJAMANAJA"
	case "Kredit Ku-KSP Pinjaman Uang":
		platform = "DFPINJAMANAJA"
	case "Uang Bijak-KSP Pinjaman Uang":
		platform = "DFPINJAMANAJA"
	case "DF-KSP Pinjaman Uang":
		platform = "DFPINJAMANAJA"
	case "dc-KSP Pinjaman Uang":
		platform = "DFPINJAMANAJA"
	case "oc-KSP Pinjaman Uang":
		platform = "DFPINJAMANAJA"
	case "KA-KSP Pinjaman Uang":
		platform = "DFPINJAMANAJA"
	case "Tunai Bijak-KSP Dompet Dana":
		platform = "DFDANASAKU"
	case "Uang Ku-KSP Dompet Dana":
		platform = "DFDANASAKU"
	case "Money Go-KSP Dompet Dana":
		platform = "DFDANASAKU"
	case "KTA Bijak-KSP Dompet Dana":
		platform = "DFDANASAKU"
	case "Kredit Ku-KSP Dompet Dana":
		platform = "DFDANASAKU"
	case "Tunai Pinjaman-KSP Dompet Dana":
		platform = "DFDANASAKU"
	case "Dompet Pintar-KSP Dompet Dana":
		platform = "DFDANASAKU"
	case "Uang Bijak-KSP Dompet Dana":
		platform = "DFDANASAKU"
	case "DF-KSP Dompet Dana":
		platform = "DFDANASAKU"
	case "oc-KSP Dompet Dana":
		platform = "DFDANASAKU"
	case "Pinjaman Kilat-KSP Dompet Dana":
		platform = "DFDANASAKU"
	case "Pinjaman Online-KSP Dompet Dana":
		platform = "DFDANASAKU"
	case "Pinjaman Kilat-KSP Pinjaman Uang":
		platform = "DFPINJAMANAJA"
	case "Pinjaman Online-KSP Pinjaman Uang":
		platform = "DFPINJAMANAJA"
	case "dc-KSP Dompet Dana":
		platform = "KPDANASAKU"
	default:
		runtime.CLog.Debug("获取用户数据", "营销任务中该discname:%v 不在范围内,i=:%v", discname, i)
		a := i % 3
		if a == 0 {
			//发出报警
			SendDingTalkMessage("营销任务中此discname：" + discname + "无映射关系")
		}
		return "", "无对应app"
	}
	return platform, ""
}

func GetTaskInfo(company string, i int) (string, string, string) {
	var taskname string
	var start_time string
	var end_time string

	if company == "duke" {
		taskname = "P0_ID_sti_0524_1230"
		start_time = " 08:00:00"
		end_time = " 09:00:00"
	}

	if company == "Ruidao" && i == 1 {
		taskname = "ruidao-0524-1"
		start_time = " 11:20:00"
		end_time = " 12:20:00"
	}

	if company == "Ruidao" && i == 2 {
		taskname = "ruidao-0524-2"
		start_time = " 16:00:00"
		end_time = " 17:00:00"
	}
	return taskname, start_time, end_time
}
