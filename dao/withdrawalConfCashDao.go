package dao

import (
	"addPlan/model"
	"addPlan/share"
	"errors"
	"github.com/jinzhu/gorm"
)

type ConfCashDao struct {
	engine *gorm.DB
}

func NewConfCashDao() *ConfCashDao {
	return &ConfCashDao{
		engine: share.YunHaiDBGorm,
	}
}

// 保存
func (d *ConfCashDao) SaveData(data *model.WithdrawalConfCashReq) (int, error) {
	newData := new(model.WithdrawalConfCash)
	if data.Id != 0 {
		if err := d.engine.First(&newData, data.Id).Error; err != nil {
			return 0, err
		}
	} else {
		oldList, err := d.FindListBySkuOrAppId(data.Sku, data.AppId)
		if err != nil {
			return 0, err
		}
		if len(oldList) != 0 {
			return 0, errors.New("sku 已存在")
		}
		newData.Sku = data.Sku
		newData.WithdrawalConfGlobalId = data.WithdrawalConfGlobalId
	}

	newData.Sort = data.Sort
	newData.Cash = data.Cash
	newData.Points = data.Points
	newData.DailyLimit = data.DailyLimit

	err := d.engine.Save(newData).Error
	if err != nil {
		return 0, err
	}
	return newData.ID, nil
}

// 查看
func (d *ConfCashDao) GetDetail(id string) (*model.WithdrawalConfCash, error) {
	data := new(model.WithdrawalConfCash)
	err := d.engine.Where("id=?", id).Find(&data).Error
	if err != nil {
		if err == share.GormErrRecordNotFound {
			return nil, nil
		}
		return nil, err
	}

	return data, nil
}

// 获取列表
func (d *ConfCashDao) FindListByConfGlobalId(confGlobalId int) ([]model.WithdrawalConfCash, error) {
	dataList := make([]model.WithdrawalConfCash, 0)
	if err := d.engine.Where("withdrawal_conf_global_id = ?", confGlobalId).Order("sort asc").Find(&dataList).Error; err != nil {
		if err == share.GormErrRecordNotFound || err == share.GormNoRowsInResultSet {
			return nil, nil
		}
		return nil, err
	}

	return dataList, nil
}

func (d *ConfCashDao) FindListBySkuOrAppId(sku string, appId int) ([]model.WithdrawalConfCash, error) {
	dataList := make([]model.WithdrawalConfCash, 0)
	db := d.engine.Unscoped()
	if len(sku) != 0 {
		db = db.Where("sku = ?", sku)
	}

	if appId != 0 {
		db = db.Where("app_id = ?", appId)
	}
	if err := db.Order("sort asc").Find(&dataList).Error; err != nil {
		if err == share.GormErrRecordNotFound || err == share.GormNoRowsInResultSet {
			return nil, nil
		}
		return nil, err
	}

	return dataList, nil
}

// 删除
func (d *ConfCashDao) DelData(id int) error {
	return d.engine.Where("id = ?", id).Delete(&model.WithdrawalConfCash{}).Error
}
