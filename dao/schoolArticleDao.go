package dao

import (
	"addPlan/model"
	"addPlan/share"
	"fmt"
	"github.com/jinzhu/gorm"
	"math/rand"
	"time"
)

type SchoolArticlesDao struct {
	engine *gorm.DB
}

func NewSchoolArticleDao() *SchoolArticlesDao {
	return &SchoolArticlesDao{
		engine: share.YunHaiDBGorm,
	}
}

// 保存文章
func (d *SchoolArticlesDao) SaveSchoolArticle(data *model.SchoolArticle) (int, error) {
	// 浏览数，在2000~3000中随机整数为文章起始浏览数，点击即为一次浏览
	if data.ID == 0 { //新增
		rand.Seed(time.Now().Unix())
		data.ReadNumber = rand.Intn(1001) + 2000
	}
	err := d.engine.Save(data).Error
	if err != nil {
		return 0, err
	}
	return data.ID, nil
}

// 更新阅读数
func (d *SchoolArticlesDao) AddReadNumber(id int) error {
	sql := fmt.Sprintf("update school_article set read_number=read_number+1 where id = %d", id)
	return d.engine.Exec(sql).Error
}

// 查看详情
func (d *SchoolArticlesDao) GetDetail(id string) (*model.SchoolArticle, error) {
	data := new(model.SchoolArticle)
	err := d.engine.Where("id=?", id).Find(&data).Error
	if err != nil {
		if err == share.GormErrRecordNotFound {
			return nil, nil
		}
		return nil, err
	}

	return data, nil
}

// 获取列表
func (d *SchoolArticlesDao) FindInfoList(title string, articleType model.ArticleType, page, size int) ([]model.SchoolArticle, int, error) {
	if page <= 0 {
		page = 1
	}
	if size <= 0 {
		size = 10
	}

	dataList := make([]model.SchoolArticle, 0)
	var count int
	db := d.engine.Model(model.SchoolArticle{}).Select("id,created_at,title,image_url,content,article_type,read_number")
	if articleType != model.ArticleType99 {
		db = db.Where("article_type = ?", articleType)
	}
	if title != "" {
		db = db.Where("title like ?", "%"+title+"%")
	}

	if err := db.Count(&count).Error; err != nil {
		if err == share.GormErrRecordNotFound || err == share.GormNoRowsInResultSet {
			return nil, 0, nil
		}
		return nil, 0, err
	}

	if err := db.Order("created_at desc").Offset((page - 1) * size).
		Limit(size).Find(&dataList).Error; err != nil {
		if err == share.GormErrRecordNotFound || err == share.GormNoRowsInResultSet {
			return nil, 0, nil
		}
		return nil, 0, err
	}

	return dataList, count, nil
}

func (d *SchoolArticlesDao) DelSchoolArticle(id int) error {
	return d.engine.Where("id = ?", id).Delete(&model.SchoolArticle{}).Error
}
