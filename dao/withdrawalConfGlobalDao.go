package dao

import (
	"addPlan/model"
	"addPlan/share"
	"github.com/jinzhu/gorm"
)

type ConfGlobalDao struct {
	engine *gorm.DB
}

func NewConfGlobalDao() *ConfGlobalDao {
	return &ConfGlobalDao{
		engine: share.YunHaiDBGorm,
	}
}

// 保存全局配置
func (d *ConfGlobalDao) SaveData(data *model.WithdrawalConf) (int, error) {
	newData := new(model.WithdrawalConfGlobal)
	if data.Id != 0 {
		if err := d.engine.First(&newData, data.Id).Error; err != nil {
			return 0, err
		}
	}
	// 使用主键获取记录
	newData.AppId = data.AppId
	newData.Way = data.Way
	newData.Explain = data.Explain
	newData.Operator = data.Operator

	err := d.engine.Save(newData).Error
	if err != nil {
		return 0, err
	}
	return newData.ID, nil
}

// 查看全局配置
func (d *ConfGlobalDao) GetDataByAppId(appId int) (*model.WithdrawalConfGlobal, error) {
	data := new(model.WithdrawalConfGlobal)
	err := d.engine.Where("app_id=?", appId).Find(&data).Error
	if err != nil {
		if err == share.GormErrRecordNotFound {
			return nil, nil
		}
		return nil, err
	}

	return data, nil
}
