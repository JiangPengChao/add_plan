package model

type OkPacket struct {
	Code    int32       `json:"code"`
	Data    interface{} `json:"data,omitempty"`
	Message string      `json:"message,omitempty"`
}
