package model

type WithdrawalConfCash struct {
	Base
	AppId                  int    `json:"app_id" gorm:"column:app_id; type:int(11) not null comment 'appId';"`
	WithdrawalConfGlobalId int    `json:"withdrawal_conf_global_id" gorm:"column:withdrawal_conf_global_id;type:int(11) not null comment '提现全局配置id';"`
	Sku                    string `json:"sku" gorm:"column:sku;type:varchar(255) not null comment '提现金额配置SKU';"`
	Sort                   int    `json:"sort" gorm:"column:sort;type:int(10) not null comment '排序';"`
	Cash                   int    `json:"cash" gorm:"column:cash;type:int(10) not null comment '提现金额，单位分';"`
	Points                 int    `json:"points" gorm:"column:points;type:int(10) not null comment '扣除积分';"`
	DailyLimit             int    `json:"daily_limit" gorm:"column:daily_limit;type:int(10) not null comment '每日提现额度';"`
}

type WithdrawalConfCashReq struct {
	Id                     int    `json:"id"`
	AppId                  int    `json:"app_id"`
	WithdrawalConfGlobalId int    `json:"withdrawal_conf_global_id"`
	Sku                    string `json:"sku"`
	Sort                   int    `json:"sort"`
	Cash                   int    `json:"cash"`
	Points                 int    `json:"points"`
	DailyLimit             int    `json:"daily_limit"`
}
