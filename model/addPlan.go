package model

import "time"

type CsData struct {
	Id              int       `json:"id"`
	DateAt          time.Time `json:"dataAt"`
	Date            string    `json:"data"`
	Hour            string    `json:"hour"`
	Phone           string    `json:"phone"`
	ApplyTimeAt     time.Time `json:"applyTimeAt"`
	RepaymentTimeAt time.Time `json:"repaymentTimeAt"`
	ApplyTime       string    `json:"applyTime"`
	RepaymentTime   string    `json:"repaymentTime"`
	Name            string    `json:"name"`
	Account         string    `json:"account"`
	Amount          string    `json:"amount"`
	DiscName        string    `json:"discName"`
}

type ParamsDetail struct {
	Debtor     string  `json:"Debtor"`
	Mobile     string  `json:"Mobile"`
	LoanDate   string  `json:"LoanDate"`
	DueDate    string  `json:"DueDate"`
	LoanAmount float64 `json:"LoanAmount"`
	Arrears    float64 `json:"Arrears"`
	Unit       string  `json:"Unit"`
	Platform   string  `json:"Platform"`
	ExtraA     string  `json:"Extra_A"`
}

type ParamsPDetail struct {
	Customer string `json:"Customer"`
	Mobile   string `json:"Mobile"`
	Comments string `json:"Comments"`
	Platform string `json:"Platform"`
	Gender   string `json:"Gender"`
}

type PCsData struct {
	Id       int       `json:"id"`
	DateAt   time.Time `json:"dataAt"`
	Date     string    `json:"data"`
	Hour     string    `json:"hour"`
	Phone    string    `json:"phone"`
	Name     string    `json:"name"`
	Account  string    `json:"account"`
	DiscName string    `json:"discName"`
	Gender   string    `json:"gender"`
}

type PDukeLoan struct {
	Id      int       `json:"id"`
	DateAt  time.Time `json:"dataAt"`
	Date    string    `json:"data"`
	Phone   string    `json:"phone"`
	Name    string    `json:"name"`
	App     string    `json:"app"`
	Product string    `json:"product"`
}
