package model

type SchoolArticle struct {
	Base
	Title       string      `json:"title" gorm:"column:title; type:varchar(50) not null comment '标题';"`
	ImageUrl    string      `json:"image_url" gorm:"column:image_url;type:varchar(200) not null comment '图片路径';"`
	Content     string      `json:"content" gorm:"column:content;type:text not null comment '文章内容';"`
	ArticleType ArticleType `json:"article_type" gorm:"column:article_type;type:int(2) comment '文章类型：1业界动态 2案例拆解 3其他';"`
	ReadNumber  int         `json:"read_number" gorm:"column:read_number;type:int(5) comment '阅读数';"`
}

type SchoolArticleResp struct {
	ID          int         `json:"id"`
	CreatedAt   string      `json:"created_at"`
	Title       string      `json:"title" gorm:"column:title; type:varchar(50) not null comment '标题';"`
	ImageUrl    string      `json:"image_url" gorm:"column:image_url;type:varchar(100) not null comment '图片路径';"`
	Content     string      `json:"content" gorm:"column:content;type:text not null comment '文章内容';"`
	ArticleType ArticleType `json:"article_type" gorm:"column:article_type;type:int(2) comment '文章类型：1业界动态 2案例拆解 3其他';"`
	ReadNumber  int         `json:"read_number" gorm:"column:read_number;type:int(5) comment '阅读数';"`
}

type ArticleType int

const (
	ArticleType1  ArticleType = 1  //1业界动态
	ArticleType2              = 2  //2案例拆解
	ArticleType3              = 3  //3其他
	ArticleType99             = 99 //99最新文章
)

var ArticleTypeMap = map[ArticleType]bool{
	ArticleType1:  true,
	ArticleType2:  true,
	ArticleType3:  true,
	ArticleType99: true,
}
