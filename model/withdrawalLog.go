package model

type WithdrawalLog struct {
	Base
	WithdrawalApplyId int    `json:"withdrawal_apply_id" gorm:"column:withdrawal_apply_id;type:int(10) not null comment '提现申请id';"`
	Status            int    `json:"status" gorm:"column:status;type:int(2) not null comment '进行态';"`
	Comment           string `json:"comment" gorm:"column:comment;type:text comment '第三方打款回调详情';"`
	PaymentNo         string `json:"payment_no" gorm:"column:payment_no;type:varchar(60) comment '付款单号';"`
	PartnerTradeNo    string `json:"partner_trade_no" gorm:"column:partner_trade_no;type:varchar(32) comment '商户订单号';"`
}
