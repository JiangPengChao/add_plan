package model

import "time"

type GlobalContext struct {
	Server struct {
		Hostname string
		Port     string
		Env      string
		Pporf    string
	}

	AddPlan struct {
		Address string
		MaxConn int
	}

	Log struct {
		Name           string
		LogPath        string
		BufSize        int
		MaxEnableLevel int
		NeedEmail      bool
	}

	Email struct {
		Server    string
		Sender    string
		Password  string
		Receivers []string
		Subject   string
	}

	Redis struct {
		Address        string
		Password       string
		BufferCapacity int
		ConnTimeout    time.Duration
		WriteTimeout   time.Duration
		ReadTimeout    time.Duration
	}

}
