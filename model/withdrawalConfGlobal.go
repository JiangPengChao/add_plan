package model

type WithdrawalConfGlobal struct {
	Base
	AppId   int    `json:"app_id" gorm:"column:app_id; type:int(11) not null comment 'appId';"`
	Way     string `json:"way" gorm:"column:way;type:varchar(5) not null comment '提现方式：1微信 2支付宝';"`
	Explain string `json:"explain" gorm:"column:explain;type:text comment '提现说明';"`
}

type WithdrawalConf struct {
	Id           int                  `json:"id"`
	AppId        int                  `json:"app_id"`
	Way          string               `json:"way"`
	Explain      string               `json:"explain"`
	Operator     string               `json:"operator"`
	ConfCashList []WithdrawalConfCash `json:"conf_cash_list"`
}
